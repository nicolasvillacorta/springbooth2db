package com.nico.pruebah2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaH2DbApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaH2DbApplication.class, args);
	}

}
