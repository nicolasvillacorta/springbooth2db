package com.nico.pruebah2.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nico.pruebah2.models.Empleado;

@Repository
public interface EmpleadoDao extends JpaRepository<Empleado, Integer>{
	public List<Empleado> findByDni(String dni);
}
