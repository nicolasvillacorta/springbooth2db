package com.nico.pruebah2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.nico.pruebah2.dao.EmpleadoDao;
import com.nico.pruebah2.models.Empleado;

@RestController
public class MainController {

	@Autowired
	EmpleadoDao empleadoDao;
	
	@PostMapping("/empleado")
	public Empleado saveEmp(@RequestBody Empleado empleado) {
		return empleadoDao.save(empleado);
	}
	
	@GetMapping("/empleado/{id}")
	public Empleado empByDni(@PathVariable int id){
		return empleadoDao.findById(id).orElse(null);
	}
	
}
