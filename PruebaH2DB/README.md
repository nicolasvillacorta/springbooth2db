Con solo tener la dependencia de H2, JPA le va a pegar.

Para acceder a la consola de H2 hay que entrar a "localhost:8080/h2-console".
Al acceder en "JDBC URL" hay que poner "jdbc:h2:mem:testdb" para que genere la base de datos en memoria, y logeamos.

Las credenciales default son; -username: sa -password: (vacia). En este caso yo las cambie en el properties.